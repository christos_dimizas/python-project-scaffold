import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="project",
    version="0.0.1",
    author="Author",
    author_email="",
    description="Scaffold project with basic packages",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    install_requires=[
        'pandas',
        'numpy',
        'matplotlib',
    ],
    classifiers=[],
)
