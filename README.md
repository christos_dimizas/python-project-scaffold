
# **Initial setup**

Start by creating a virtual environment (optional: inside a .virtualenv folder in the project) 
using the python version that feet your project and activate it,
by running `activate` inside the /Scripts folder of the virtual env folder.

For example:
- Go to installation folder, from terminal, of the desired python version
- Run `python -m venv [the/path/of/the/virtual env]`
- Go to the `[the/path/of/the/virtual env]`
- Run `activate` (to deactivate run `deactivate`)

#### Set interpreter

From your IDE set the python interpreter to be the virtual environment you have just created.


###### Note:

If pip version is old run:

`python -m pip install --upgrade pip`



# Supported Libraries

Currently the supported libraries are:

- Pandas
- Numpy

# Dependencies installation

#### Method 1

In order to install dependencies on virtual environment run:

`pip install .`

This will reade the 'install_requires' property in the _setup.py_ file and will fetch all listed packages

If in this point you want to create an updated version of the _reqs.txt_ file, you could run: `pip freeze > reqs.txt`. 

#### Method 2

You can install packages from _reqs.txt_ file too.

File _reqs.txt_ contains the list of the project dependencies, including their versions we need.

In order to install desired packages using this way you can run: 

`pip install -r reqs.txt`

 